package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
)

func main() {
	var port = os.Getenv("PORT")
	if port == "" {
		port = ":8000"
	}
	if len(os.Args) > 1 {
		port = os.Args[1]
	}
	if !strings.HasPrefix(port, ":") {
		port = ":" + port
	}
	http.HandleFunc("/hello", func(writer http.ResponseWriter, req *http.Request) {
		fmt.Fprintf(writer, "<h1>This is an example server without encryption.</h1>")
	})
	http.HandleFunc("/redirect", func(writer http.ResponseWriter, req *http.Request) {
		http.Redirect(writer, req, "http://go-simplehttp.herokuapp.com/hello", 301)
	})
	log.Fatal(http.ListenAndServe(port, nil))
}
