# Simple http server 

A simple http server and a template for godep project ready to be deployed to heroku.

## Build

```
GOOS=windows GOARCH=amd64 go build
GOOS=linux GOARCH=amd64 go build -o simplehttp.linux
GOOS=GOOS=darwin GOARCH=amd64 go build -o simplehttp.mac
```

Or just go to the download section and get the binaries.

Or `godep go install ./...`

## Run

```
PORT=4545 ./simplehttp # set the port by env var
./simplehttp           # if the PORT env var is not set, then use 8000 by default
./simplehttp 8080      # if the first cmd arg is present, then it's used as port
```

Port is 80 by default.

## Test in browser

Go to [http://127.0.0.1:8080/hello](http://127.0.0.1:8080/hello), the port number may vary.