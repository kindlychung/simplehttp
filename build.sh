#!/usr/bin/env bash

echo "Building for Windows"
GOOS=windows GOARCH=amd64 go build
echo "Building for Linux"
GOOS=linux GOARCH=amd64 go build -o simplehttp.linux
echo "Building for Mac"
GOOS=darwin GOARCH=amd64 go build -o simplehttp.mac
